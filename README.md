# Connected Devices - Java

Connected Devices - Java repository.

Please see the LICENSE files located in ./src/main/java/com/labbenchstudios
and ./src/test/java/com/labbenchstudios for information on using the source
code provided in, and under, ./src/main/java/com/labbenchstudios and
./src/test/java/com/labbenchstudios.

##Instructions

Update the pom.xml by replacing 'schooldomain.studentname.connecteddevices',
and all instances of 'schooldomain', and 'studentname' with your content.
E.g. 'schooldomain' should be 'neu', and 'studentname' will be your one
word name (don't use hyphens, spaces, periods, or other special characters).
=======
**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*
