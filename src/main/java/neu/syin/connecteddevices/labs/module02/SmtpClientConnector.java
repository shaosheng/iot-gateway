package neu.syin.connecteddevices.labs.module02;

import java.util.Date;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;
import com.labbenchstudios.edu.connecteddevices.common.ConfigUtil;

public class SmtpClientConnector {

	private static final Logger _Logger = Logger.getLogger(SmtpClientConnector.class.getSimpleName());

	private Session session;
	private String fromAddr;
	private String toAddr;
	private String host;
	private String username;
	private String password;
	private String port;
	private String auth;

	public SmtpClientConnector() {
		super();
		initClient();
	}

	/*
	 * get config from file and set property
	 * initialize stmp client and authenticate
	 * */
	public void initClient() {
		ConfigUtil.getInstance()
				.loadConfig("/Users/shaoshengyin/git/csye6510/iot-gateway/config/ConnectedDevicesConfig.props");
		fromAddr = ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.FROM_ADDRESS_KEY);
		toAddr = ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.TO_ADDRESS_KEY);
		host = ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.HOST_KEY);
		username = ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.FROM_ADDRESS_KEY);
		password = ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION,
				ConfigConst.USER_AUTH_TOKEN_KEY);
		port = ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.PORT_KEY);
		auth = ConfigUtil.getInstance().getProperty(ConfigConst.SMTP_CLOUD_SECTION, ConfigConst.ENABLE_AUTH_KEY);
		Properties props = new Properties();

		props.put("mail.smtp.host", host); // SMTP Host
		props.put("mail.smtp.socketFactory.port", port); // SSL Port
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); // SSL Factory Class
		props.put("mail.smtp.auth", auth); // Enabling SMTP Authentication
		props.put("mail.smtp.port", port); // SMTP Port

		session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {

			// override the getPasswordAuthentication
			// method
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		_Logger.info("email session created");
	}

	/*
	 * wrap send email method
	 * */
	public void publicMessage(String msg) {
		// TODO Auto-generated method stub
		sendEmail(session, toAddr, "Sensor Data", msg);
	}

	/**
	 * Utility method to send simple HTML email
	 * 
	 * @param session
	 * @param toEmail
	 * @param subject
	 * @param body
	 */
	public static void sendEmail(Session session, String toEmail, String subject, String body) {
		try {
			MimeMessage msg = new MimeMessage(session);
			// set message headers
			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");

			msg.setFrom(new InternetAddress("no_reply@example.com", "NoReply-JD"));
			msg.setReplyTo(InternetAddress.parse("no_reply@example.com", false));
			msg.setSubject(subject, "UTF-8");
			msg.setText(body, "UTF-8");
			msg.setSentDate(new Date());
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
			_Logger.info("Message is ready");

			Transport.send(msg);

			_Logger.info("EMail Sent Successfully!!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
