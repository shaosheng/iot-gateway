package neu.syin.connecteddevices.labs.module02;

import java.util.Random;
import java.util.logging.Logger;

import neu.syin.connecteddevices.labs.common.SensorData;

public class TempSensorEmulator implements Runnable{
	
	private static final Logger _Logger = Logger.getLogger(TempSensorEmulator.class.getSimpleName());
	
	private long rateInSec;
	private String description;
	
	private SensorData sensorData = new SensorData();
	private SmtpClientConnector stmpclient = new SmtpClientConnector();
	
	public int range = 30;
	public float threshold = 5.0f;
	public String DEFAULT_DES = "Temperature";
	public long DEFAULT_RATE = 10L;

	//constructor
	public TempSensorEmulator() {
		this.description = DEFAULT_DES;
		this.rateInSec = DEFAULT_RATE;
	}
	
	//constructor
	public TempSensorEmulator(String string, long _pollCycle) {
		// TODO Auto-generated constructor stub
		this.description = string;
		this.rateInSec = _pollCycle;
	}

	//generate temperature in the range
	//@return float 
	public float generateTemp() {
        Random rand = new Random();
        float newTemp = rand.nextFloat() * range;
        return newTemp; 
	}
 
	public void run() {
		// TODO Auto-generated method stub
		float newTemp = this.generateTemp();
		_Logger.info("current temperature" + newTemp);
		this.sensorData.addValue(newTemp);
		float diff = Math.abs(this.sensorData.getAvgValue() - newTemp);
		if (diff >= this.threshold) {
			String msg = "Current value exceeds threshold with difference" + diff;
			_Logger.info(msg);
			//trigger email notification
			stmpclient.publicMessage(msg);
		}
	}
	

}
