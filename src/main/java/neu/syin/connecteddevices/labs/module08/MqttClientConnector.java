package neu.syin.connecteddevices.labs.module08;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.sql.Timestamp;
import java.util.logging.Logger;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;

public class MqttClientConnector implements MqttCallback {

	private static final Logger logger = Logger.getLogger(MqttClientConnector.class.getName());
	private String mqttClientID;
	private String brokerAddress;
	private MqttClient mqttClient;
	private String protocol = ConfigConst.DEFAULT_MQTT_PROTOCOL;
	private String host = "things.ubidots.com";
	private int port = ConfigConst.DEFAULT_MQTT_PORT;
	private boolean quietMode;
	private int qos;
	private MqttConnectOptions conOpt;
	private String userName;
	private String pemFileName;
	private boolean isSecureConn = false;
	
	public MqttClientConnector() {
		this(null, false);
	}

	public MqttClientConnector(String host, boolean isSecure) {
		super();
		if (host != null && host.trim().length() > 0) {
			this.host = host;
		}
		this.isSecureConn = isSecure;
		mqttClientID = mqttClient.generateClientId();
		// create brkoer url
		brokerAddress = protocol + "://" + host + ":" + port;
		log("Broker URL for conneciting" + brokerAddress);

	}
	
	//for SSL connection
	public MqttClientConnector(String host, String userName, String pemFileName) {
		super();
		if (host != null && host.trim().length() > 0) {
			this.host = host;
		}

		if (userName != null && userName.trim().length() > 0) {
			this.userName = userName;
		}

		if (pemFileName != null) {
			File file = new File(pemFileName);

			if (file.exists()) {
				this.protocol = "ssl";
				this.port = 8883;
				this.pemFileName = pemFileName;
				this.isSecureConn = true;
				log("PEM file valid. Using secure connection: " + pemFileName);
			} else {
				log("PEM file invalid. Using insecure connection: " + pemFileName);
			}
		}

		mqttClientID = MqttClient.generateClientId();
		brokerAddress = protocol + "://" + host + ":" + port;
		log("MqttClientID :" + mqttClientID);
		log("Using URL for broker conn: " + brokerAddress);

	}

	public void connect() {
		// TODO Auto-generated method stub
		if (mqttClient == null) {
			MemoryPersistence persistence = new MemoryPersistence();
			conOpt = new MqttConnectOptions();
			try {
				mqttClient = new MqttClient(brokerAddress, mqttClientID, persistence);
				conOpt.setCleanSession(true);
				if (userName != null) {
					conOpt.setUserName(userName);
				}
				if (this.isSecureConn) {
					initSecureConnection(conOpt);
				}
				
				// Sets the connection timeout value.
				conOpt.setConnectionTimeout(30);
				// Sets the "keep alive" interval.
				conOpt.setKeepAliveInterval(65);
				mqttClient.setCallback(this);
				mqttClient.connect(conOpt);
				System.out.println("Connect to Broker:" + brokerAddress);
			} catch (MqttException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			System.out.println("MqttClient has not been intialized");
		}

	}
	
	/**
	 * initialize secure connection
	 *@param options 
	 */
	private void initSecureConnection(MqttConnectOptions options)
    {
	 try {
		 
		 	SSLContext sslContext = SSLContext.getInstance("SSL");
		 	KeyStore keyStore = readCertificate();
		    TrustManagerFactory trustManagerFactory =
		    TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		    trustManagerFactory.init(keyStore);
		    sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
		    options.setSocketFactory(sslContext.getSocketFactory());
		    
	 } catch (Exception e) 
	 {
	 
		 log("Failed to initialize secure MQTT connection "+e.getMessage());

	 }
	 
	 }
	
	/**
	 * read certificate
	 * @return keyStore
	 * */
	 private KeyStore readCertificate() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException
	 {
		 
		 KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
		 FileInputStream fis = new FileInputStream(pemFileName);
		 BufferedInputStream bis = new BufferedInputStream(fis);
		 CertificateFactory cf = CertificateFactory.getInstance("X.509");
		 ks.load(null);
		 while (bis.available() > 0) 
		 {
			 Certificate cert = cf.generateCertificate(bis);
			 ks.setCertificateEntry("adk_store" + bis.available(), cert);
	     }
	     return ks;
	 }

	public void subscribe(String topicName, int qos) throws MqttException {
		
		mqttClient.subscribe(topicName, qos);
		// Subscribe to the requested topic
		log("Subscribing to topic \"" + topicName + "\" qos " + qos);

//		// Continue waiting for messages until the Enter is pressed
//		log("Press <Enter> to exit");
//		try {
//			System.in.read();
//		} catch (IOException e) {
//			// If we can't read we'll just exit
//		}
	}

	/**
	 * Publish / send a message to an MQTT server
	 * 
	 * @param topicName the name of the topic to publish to
	 * @param qos       the quality of service to delivery the message at (0,1,2)
	 * @param payload   the set of bytes to send to the MQTT server
	 * @throws MqttException
	 */
	public void publish(String topicName, int qos, byte[] payload) throws MqttException {
		// TODO Auto-generated method stub
		log("Connecting to " + brokerAddress + "with clientID " + mqttClient.getClientId());
		String time = new Timestamp(System.currentTimeMillis()).toString();
		log("Publishing at: " + time + " to topic \"" + topicName + "\" qos " + qos);

		// Create and configure a message
		MqttMessage message = new MqttMessage(payload);
		message.setQos(qos);

		// Send the message to the server, control is not returned until
		// it has been delivered to the server meeting the specified
		// quality of service.
		mqttClient.publish(topicName, message);
	}

	/**
	 * Utility method to handle logging. If 'quietMode' is set, this method does
	 * nothing
	 * 
	 * @param message the message to log
	 */
	private void log(String message) {
		if (!quietMode) {
			System.out.println(message);
		}
	}

	public void disconnect() throws MqttException {
		// TODO Auto-generated method stub
		mqttClient.disconnect();
		log("Disconnected");
	}

	/**
	 * when connection lost, log cause
	 * 
	 * @param cause
	 */
	public void connectionLost(Throwable cause) {
		// TODO Auto-generated method stub
		log("Connection to " + brokerAddress + " lost!" + cause);
		System.exit(1);
	}

	/**
	 * @param message
	 * @param topic
	 */
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		log("Message arrived: " + topic + ", id " + message.getId());
		log("Message plyload: "+ message.toString());

	}

	// function need to implement
	public void deliveryComplete(IMqttDeliveryToken token) {

	}

}
