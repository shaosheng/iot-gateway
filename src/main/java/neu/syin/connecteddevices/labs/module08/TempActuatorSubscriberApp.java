package neu.syin.connecteddevices.labs.module08;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.paho.client.mqttv3.MqttException;

public class TempActuatorSubscriberApp {
	private static final Logger logger = Logger.getLogger(TempActuatorSubscriberApp.class.getName());
	private static TempActuatorSubscriberApp app;
	private MqttClientConnector client;
	private String host = "industrial.ubidots.com";
//	private String host = "things.ubidots.com";
	private String userName = "BBFF-uhDtFb6UuNlmibMFspKhl9o4TefQg2";
	private String pemFileName = "/Users/shaoshengyin/git/csye6510/iot-gateway/config/ubidots_cert.pem";

	/*
	 * main method
	 */
	public static void main(String[] args) {
		app = new TempActuatorSubscriberApp();
		try {
			app.start();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to start app.", e);
		}
	}

	/*
	 * subscribe two topic using mqtt client
	 * */
	public void start() throws MqttException {
		// Create a new MqttClient:_Client and connection
		client = new MqttClientConnector(host, userName, pemFileName);
		client.connect();
		String topic1 = "/v1.6/devices/csye6510_yin/tempactuator";
		client.subscribe(topic1, 2);
		String topic2 = "/v1.6/devices/csye6510_yin/tempsensor";
		client.subscribe(topic2, 2);
	}
}
