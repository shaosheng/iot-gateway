package neu.syin.connecteddevices.labs.module05;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.logging.Logger;

import com.google.gson.Gson;

import neu.syin.connecteddevices.labs.common.DataUtil;
import neu.syin.connecteddevices.labs.common.SensorData;
import neu.syin.connecteddevices.labs.module02.SmtpClientConnector;

public class TempSensorEmulator implements Runnable{
	
	private static final Logger _Logger = Logger.getLogger(TempSensorEmulator.class.getSimpleName());
	
	private long rateInSec;
	private String description;
	
	private SensorData sensorData = new SensorData();
	private SmtpClientConnector stmpclient = new SmtpClientConnector();
	private DataUtil dataUtil = new DataUtil();
	
	public int range = 30;
	public float threshold = 5.0f;
	public String DEFAULT_DES = "Temperature";
	public long DEFAULT_RATE = 10L;

	//constructor
	public TempSensorEmulator() {
		this.description = DEFAULT_DES;
		this.rateInSec = DEFAULT_RATE;
	}
	
	//constructor
	public TempSensorEmulator(String string, long _pollCycle) {
		// TODO Auto-generated constructor stub
		this.description = string;
		this.rateInSec = _pollCycle;
	}

	//generate temperature in the range
	//@return float 
	public float generateTemp() {
        Random rand = new Random();
        float newTemp = rand.nextFloat() * range;
        return newTemp; 
	}
	
	public SensorData readFile() throws IOException {
		String filePath = "/Users/shaoshengyin/Desktop/result.json";
		BufferedReader br = new BufferedReader(new FileReader(filePath));
		String jsonData = new String();
		for (String line; (line = br.readLine()) != null; jsonData += line);
		SensorData tmp = dataUtil.jsonToSensorData(jsonData);
		return tmp;
		
	}
	
	public void writeFile(SensorData sensorData) throws IOException {
		Gson gson = new Gson();
		String filePath = "/Users/shaoshengyin/Desktop/result2.json";
		System.out.println("start to write file");
		String tmp = dataUtil.sensorDataToJson(sensorData);
		FileWriter writer = new FileWriter(filePath);
		writer.write(tmp);
		writer.flush();
		writer.close();
	}
 
	public void run() {
		// TODO Auto-generated method stub
		float newTemp = this.generateTemp();
		_Logger.info("current temperature" + newTemp);
		this.sensorData.addValue(newTemp);
		float diff = Math.abs(this.sensorData.getAvgValue() - newTemp);
		if (diff >= this.threshold) {
			String msg = "Current value exceeds threshold with difference" + diff;
			_Logger.info(msg);
			//trigger email notification
			stmpclient.publicMessage(msg);
		}
		try {
			SensorData tmp = readFile();
			System.out.println(tmp.toString());
			System.out.println("read file done");
			writeFile(tmp);
			System.out.println("create file done");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
