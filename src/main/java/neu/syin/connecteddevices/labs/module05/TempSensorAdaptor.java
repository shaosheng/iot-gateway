package neu.syin.connecteddevices.labs.module05;

import java.util.logging.Logger;

import com.labbenchstudios.edu.connecteddevices.common.DevicePollingManager;

import neu.syin.connecteddevices.labs.module01.SystemCpuUtilTask;

public class TempSensorAdaptor {
	
	private static final Logger _Logger = Logger.getLogger(TempSensorAdaptor.class.getSimpleName());
	private DevicePollingManager _pollManager;
	private long _pollCycle;
	
	
	/*
	 * constructor
	 * */
	public TempSensorAdaptor(long pollCycle) {
		super();
		//set interval value
		if (pollCycle >= 1L) {
			_pollCycle = pollCycle;
		}
		//create task
		_pollManager = new DevicePollingManager(1);
	}


	/*
	 * reference module01 code, create polling manager
	 * and create emulator object
	 * */
	public void startPolling() {
		// TODO Auto-generated method stub
		_Logger.info("Creating and scheduling Temperature poller...");
		_pollManager.schedulePollingTask(new TempSensorEmulator("Generate Temp", _pollCycle), _pollCycle);
		
	}
	
	public void stopPolling() {
		_pollManager.stopPolling();
	}

}
