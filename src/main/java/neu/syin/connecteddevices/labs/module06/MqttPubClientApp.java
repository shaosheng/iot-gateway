package neu.syin.connecteddevices.labs.module06;

import java.util.logging.Logger;

import org.eclipse.paho.client.mqttv3.MqttException;

import com.google.gson.Gson;

import neu.syin.connecteddevices.labs.common.SensorData;

public class MqttPubClientApp {
	
	private static final Logger logger = Logger.getLogger(MqttPubClientApp.class.getName());
	private static MqttPubClientApp app;
	private MqttClientConnector mqttClient;

	//main entrance, start app
	public static void main(String[] args) {
		app = new MqttPubClientApp();
		try {
			app.start();
		}catch (Exception e) {
			e.printStackTrace();
			logger.info("failed to start publish app");
		}
	}
	

	public MqttPubClientApp() {
		super();
	}
	
	public void start() throws MqttException {
		// TODO Auto-generated method stub
		mqttClient = new MqttClientConnector("127.0.0.1", false);
		mqttClient.connect();
		
		String topicName = "test";
		
		SensorData sensorData = new SensorData();
		sensorData.addValue(18.0f);
		sensorData.setName("tempterature");
		Gson gson = new Gson();
		logger.info("Log SensorData");
		logger.info(sensorData.toString());
		
		//convert to json format
		String payload = gson.toJson(sensorData);
		logger.info("Log json string: " + payload);
		
		//publish payload in QoS level 2
		mqttClient.publish(topicName, 2, payload.getBytes());
		mqttClient.disconnect();
		
	}

}
