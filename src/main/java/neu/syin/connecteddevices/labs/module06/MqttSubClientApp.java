package neu.syin.connecteddevices.labs.module06;

import java.util.logging.Logger;

import org.eclipse.paho.client.mqttv3.MqttException;

public class MqttSubClientApp {
	
	private static final Logger logger = Logger.getLogger(MqttSubClientApp.class.getName());
	private static MqttSubClientApp app;
	private MqttClientConnector mqttClient;
	
	public static void main(String[] args) {
		app = new MqttSubClientApp();
		try {
			app.start();
		}catch (Exception e) {
			e.printStackTrace();
			logger.info("failed to start subscirbe app");
		}
	}
	
	public void start() throws MqttException {
		String topicName = "test";
		mqttClient.connect();
		mqttClient.subscribe(topicName, 2);
	}

}
