package neu.syin.connecteddevices.labs.common;

import java.text.SimpleDateFormat;
import java.util.Date;
/*
 * this is ActuatorData class, which is for 
 * get a set of value
 */
public class ActuatorData {
	
    String timeStamp;              // create an instance of timeStamp
    String name = "Temperature";   // create an instance of String
    boolean hasError = false;      // set hasError with false
    int command = 0;               // set command with 0
    int errCode = 0;               // set errCode with 0
    int statusCode = 0;            // set statues with 0
    String stateData = null;       // set state with 0
    double curValue = 0.0;         // set curValue with 0.0
    /*
     * ActuatorData constructor
     */
    public ActuatorData() {
    	// create an instance of Date
    	Date now = new Date();
    	// create an instance of SimpleDateFormat
    	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    	this.timeStamp = ft.format(now);
    }
    
    /*
     * @return: String
     */
	public String getName() {
		return name;
	}

	/*
	 * @param name : String
	 */
	public void setName(String name) {
		this.name = name;
	}

	/*
	 * @return: boolean
	 */
	public boolean isHasError() {
		return hasError;
	}

	/*
	 * @return: integer
	 */
	public int getCommand() {
		return command;
	}

	/*
	 * @param command : integer
	 */
	public void setCommand(int command) {
		this.command = command;
	}

	/*
	 * @return: integer
	 */
	public int getErrCode() {
		return errCode;
	}

	/*
	 * @param errCode : integer
	 */
	public void setErrCode(int errCode) {
		this.errCode = errCode;
		if(this.errCode != 0) {
			this.hasError = true;
		}
		else {
			this.hasError = false;
		}
	}

	/*
	 * @return: integer
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/*
	 * @param statusCode : integer
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	/*
	 * @return: String
	 */
	public String getStateData() {
		return stateData;
	}

	/*
	 * @param stateData : String
	 */
	public void setStateData(String stateData) {
		this.stateData = stateData;
	}

	/*
	 * @return: Double
	 */
	public double getCurValue() {
		return curValue;
	}

	/*
	 * @param curValue : Double
	 */
	public void setCurValue(double curValue) {
		this.curValue = curValue;
	}
    
	/*
	 * @param actuatorData : ActuatorData
	 */
	public void updateData(ActuatorData actuatorData) {
		this.command = actuatorData.getCommand();
		this.statusCode = actuatorData.getStatusCode();
		this.errCode = actuatorData.getErrCode();
		this.stateData = actuatorData.getStateData();
		this.curValue = actuatorData.getCurValue();
	}
	
	/*
	 * the function is to get current time
	 */
	public void updateTimeStamp() {
    	Date now = new Date();
    	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    	this.timeStamp = ft.format(now);
	}
	
    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
	public String toString() {
    	StringBuilder info = new StringBuilder(" " + this.getName() + ":\n");
    	info.append("\n" + "        Time: " + this.timeStamp + "\n");
    	info.append("\n" + "        Command: " + this.getCommand() + "\n");
    	info.append("\n" + "        Has Error: " + this.isHasError() + "\n");
    	info.append("\n" + "        Status Code: " + this.getStatusCode() + "\n");
    	info.append("\n" + "        Error Code: " + this.getErrCode() + "\n");
    	info.append("\n" + "        State Data: " + this.getStateData() + "\n");
    	info.append("\n" + "        Current Value: " + this.getCurValue());
    	return info.toString();
    }
    
    /*
     * this function is to show details
     */
	public void showInfo() {
    	System.out.println(this.toString());
    }

}


