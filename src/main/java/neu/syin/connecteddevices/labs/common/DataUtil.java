package neu.syin.connecteddevices.labs.common;

import com.google.gson.Gson;


/**
 * @author shaoshengyin
 *
 */
public class DataUtil {
	
	//Default constructor
	public DataUtil() {}
	
	/*
	 * the function is to convert sensor data to JSON
	 * @param sensorData : SensorData
	 * @return: String
	 */
	public String sensorDataToJson(SensorData sensorData)
	{
		// create an instance of String
		String jsonData = null;
	    if(sensorData != null) {
	    	// create an instance of Gson
	    	Gson gson = new Gson();
	        // get jsonData
	    	jsonData = gson.toJson(sensorData);
	        System.out.println("converting SensorData object to JSON string...");
	    }
	    return jsonData;
	}
	
	/*
	 * the function is to convert JSON to sensor data
	 * @param jsonData : String
	 * @return: SensorData
	 */
	public SensorData jsonToSensorData(String jsonData) {
		// create an instance of SenseData
		SensorData sensorData = new SensorData();
		if(jsonData != null && jsonData.trim().length() > 0) {
			// create an instance of GSON
			Gson gson = new Gson();
			// get JSON
			sensorData = (SensorData)gson.fromJson(jsonData, SensorData.class);
			System.out.println("converting JSON string to SensorData object...");
		}
		return sensorData;
	}
	
	/*
	 * the function is to convert actuator data to JSON
	 * @param actuatorData : AcutuatorData
	 * @return: String
	 */
	public String actuatorDataToJson(ActuatorData actuatorData) {
		// create an instance of String
		String jsonData = null;
		if(actuatorData != null) {
			// create an instance of GSON
			Gson gson = new Gson();
			// get actuator data
			jsonData = gson.toJson(actuatorData);
			System.out.println("converting ActuatorData object to JSON string...");
		}
		return jsonData;
	}
	
	/*
	 * the function is to convert JSON to actuator data
	 * @param jsonData : String
	 * @return: ActuatorData
	 */
	public ActuatorData jsonToActuatorData(String jsonData)
	{
		// create an instance of ActuatorData
		ActuatorData actuatorData = null;
	    if (jsonData != null && jsonData.trim().length() > 0) {
	    	// create an instance of GSON
	    	Gson gson = new Gson();
	        // get GSON
	    	actuatorData = (ActuatorData) gson.fromJson(jsonData, ActuatorData.class);
	        System.out.println("converting JSON string to ActuatorData object...");
	 }
	    return actuatorData;
	}
	
	
}