package neu.syin.connecteddevices.labs.common;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

public class SensorData implements Serializable{
	
	private static final Logger _Logger = Logger.getLogger(SensorData.class.getSimpleName());
	
	private String name;
	private float curValue = 0.0f;
	private float avgValue = 0.0f;
	private float minValue = 0.0f;
	private float maxValue = 0.0f;
	private float totValue = 0.0f;
	private int sampleCount = 0;
	
	private String timeStamp;
	
	public transient SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	public SensorData() {
		super();
		Date currentTime = new Date();
    	this.timeStamp = ft.format(currentTime);
	}

	public void addValue(float val) {
		updateTimeStamp();
		++this.sampleCount;
		this.curValue = val;
		this.totValue += val;
		if (this.curValue < this.minValue) {
			this.minValue = this.curValue;
		}
		if (this.curValue > this.maxValue) {
			this.maxValue = this.curValue;
		}
		if (this.totValue != 0 && this.sampleCount > 0) {
			this.avgValue = this.totValue / this.sampleCount;
		}
	}
	
	private void updateTimeStamp() {
		// TODO Auto-generated method stub
		Date currentTime = new Date();
    	this.timeStamp = ft.format(currentTime);

	}

	/*
	 * Getter and Setter
	 * */
	public float getCurValue() {
		return curValue;
	}
	public void setCurValue(float curValue) {
		this.curValue = curValue;
	}
	public float getAvgValue() {
		return avgValue;
	}
	public void setAvgValue(float avgValue) {
		this.avgValue = avgValue;
	}
	public float getMinValue() {
		return minValue;
	}
	public void setMinValue(float minValue) {
		this.minValue = minValue;
	}
	public float getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(float maxValue) {
		this.maxValue = maxValue;
	}
	public float getTotValue() {
		return totValue;
	}
	public void setTotValue(float totValue) {
		this.totValue = totValue;
	}
	public int getSampleCount() {
		return sampleCount;
	}
	public void setSampleCount(int sampleCount) {
		this.sampleCount = sampleCount;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		StringBuilder info = new StringBuilder("name: " + this.getName() + "\n");
		info.append("\n" + "        timeStamp: " + this.timeStamp + "\n");
		info.append("\n" + "        curValue: " + this.getCurValue() + "\n");
		info.append("\n" + "        avgValue: " + this.getAvgValue() + "\n");
		info.append("\n" + "        sampleCount: " + this.getSampleCount() + "\n");
		info.append("\n" + "        minValue: " + this.getMinValue() + "\n");
		info.append("\n" + "        maxValue: " + this.getMaxValue() + "\n");
    	return info.toString();
    }
	
	

}

