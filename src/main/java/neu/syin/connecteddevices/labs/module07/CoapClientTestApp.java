package neu.syin.connecteddevices.labs.module07;

import java.util.logging.Logger;

public class CoapClientTestApp {
	private static final Logger logger = Logger.getLogger(CoapClientTestApp.class.getName());
	private static CoapClientTestApp App;

	/**
	 * @param args
	 **/
	public static void main(String[] args) {
		App = new CoapClientTestApp();
		try {
			App.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// private var
	private CoapClientConnector coapClient;

	// constructor
	public CoapClientTestApp() {
		super();
	}

	/**
	 * connect to CoAP server
	 */
	public void start() {
		coapClient = new CoapClientConnector();
		coapClient.runTests("temp");
	}
}
