package neu.syin.connecteddevices.labs.module07;

public class CoapServerTestApp {
	private static CoapServerTestApp App;
	
	public static void main(String[] args) {
		
		App = new CoapServerTestApp();
		
		try {
			App.start();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	// private var's
	
	private CoapServerConnector coapServer;
	
	// constructor
	
	public CoapServerTestApp() {
		super();
	}
	
	//public methods of start coap server
	
	public void start() {
		coapServer = new CoapServerConnector();
		coapServer.start();
	}
}
