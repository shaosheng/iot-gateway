package neu.syin.connecteddevices.labs.module07;

import java.util.logging.Logger;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;

public class CoapServerConnector {

	//static logger to log information on the console
	
		private static final Logger logger = Logger.getLogger(CoapServerConnector.class.getName());
		
		// private var's , new CoapServer object
		
		private CoapServer coapServer;
		
		// constructors
		
		public CoapServerConnector() {
			super();
		}
		
		// public methods of add resource to coap server
		
		public void addResource(CoapResource resource) {
			if(resource != null) {
				coapServer.add(resource);
			}
	 	}
		
		// the start method to create coap server instance and temp handler
		public void start() {
			if(coapServer == null) {
				logger.info("Creating CoAP server instance and 'temp' handler...");
				// create coap server
				coapServer = new CoapServer();								
				TempResourceHandler tempHandler = new TempResourceHandler();
				// add tempHandler to coap server
				coapServer.add(tempHandler);								
				
			}
			logger.info("Starting CoAP server...");
			coapServer.start();
		}
		
		// the stop method to stop coap server
		public void stop() {
			logger.info("Stopping CoAP server...");
			
			coapServer.stop();
		}


}
