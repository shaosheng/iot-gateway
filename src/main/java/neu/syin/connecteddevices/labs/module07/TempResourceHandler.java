package neu.syin.connecteddevices.labs.module07;

import java.util.logging.Logger;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;

import com.google.gson.Gson;

import neu.syin.connecteddevices.labs.common.DataUtil;
import neu.syin.connecteddevices.labs.common.SensorData;

public class TempResourceHandler extends CoapResource {

	private static final Logger logger = Logger.getLogger(TempResourceHandler.class.getName());
	// create SensorData instance
	private SensorData sd = new SensorData();
	// create Gson instance
	private Gson gson = new Gson();
	private DataUtil dataUtil = new DataUtil();
	
	// default constructor
	public TempResourceHandler() {
		super("temp");
	}

	// constructor with params name
	public TempResourceHandler(String name) {
		super(name);
	}

	// constructor with params name and visible
	public TempResourceHandler(String name, boolean visible) {
		super(name, visible);
	}

	@Override
	public void handleGET(CoapExchange exchange) {
		// TODO Auto-generated method stub
		exchange.respond(ResponseCode.VALID, "Get wokred");
		logger.info("Received Get request from client");
	}

	@Override
	public void handlePOST(CoapExchange exchange) {
		// TODO Auto-generated method stub
		String requestText = exchange.getRequestText();
		logger.info("Requset Text : " + requestText);
		sd = dataUtil.jsonToSensorData(requestText);
		logger.info("Transfer JSON into SensorData instance: ");
		logger.info(sd.toString());
		logger.info("Transfer SensorData instance into JSON format: ");
		String payload = gson.toJson(sd);						
		logger.info(payload);
		exchange.respond(ResponseCode.CONTENT, payload);
	}

	@Override
	public void handlePUT(CoapExchange exchange) {
		// TODO Auto-generated method stub
		exchange.respond(ResponseCode.VALID, "Put wokred");
		logger.info("Received Put request from client");	}

	@Override
	public void handleDELETE(CoapExchange exchange) {
		// TODO Auto-generated method stub
		exchange.respond(ResponseCode.VALID, "Delete wokred");
		logger.info("Received Delete request from client");	}

}
