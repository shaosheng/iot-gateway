package neu.syin.connecteddevices.labs.module07;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.WebLink;
import org.eclipse.californium.core.coap.MediaTypeRegistry;

import com.google.gson.Gson;
import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;

import neu.syin.connecteddevices.labs.common.SensorData;

public class CoapClientConnector {

	String host;
	String protocol;
	int port;
	String serverAddress;
	private static final Logger logger = Logger.getLogger(CoapClientConnector.class.getName());
	private boolean isInitialized;
	CoapClient clientConn;

	// default constructor, set coap server
	public CoapClientConnector() {
		this(ConfigConst.DEFAULT_COAP_SERVER, false);
	}

	public CoapClientConnector(String defaultCoapServer, boolean isSecure) {
		super();

		if (isSecure) {
			protocol = ConfigConst.SECURE_COAP_PROTOCOL;
			port = ConfigConst.SECURE_COAP_PORT;
		} else {
			protocol = ConfigConst.DEFAULT_COAP_PROTOCOL;
			port = ConfigConst.DEFAULT_COAP_PORT;
		}

		if (host != null && host.trim().length() > 0) {
			host = defaultCoapServer;
		} else {
			host = ConfigConst.DEFAULT_COAP_SERVER;
		}

		// construct url
		serverAddress = protocol + "://" + host + ":" + port;
		logger.info("Using Url for Coap server connection : " + serverAddress);
	}

	// runTests method to transfer sensorData instance into JSON format
	// send PING, GET, PUT, POST, DELETE requests
	public void runTests(String resourceName) {
		try {
			isInitialized = false;
			initClient(resourceName); // initial client
			logger.info("Current URI: " + getCurrentUri());
			SensorData sd = new SensorData(); // new sensordata object
			sd.addValue(19.0f);
			sd.setName("test");
			Gson gson = new Gson();
			String payload = gson.toJson(sd); // transfer SensorData instance into JSON format
			logger.info("payload : " + payload);
			pingServer();
			discoverResources();
			sendGetRequest();
			sendGetRequest(true);
			sendPostRequest(payload, false);
			sendPostRequest(payload, true);
			sendPutRequest(payload, false);
			sendPutRequest(payload, true);
			sendDeleteRequest();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to issue request to CoAP server.", e);
		}
	}

	// test connection
	public void discoverResources() {
		logger.info("Issuing discover...");
		initClient();
		Set<WebLink> wlSet = clientConn.discover();

		if (wlSet != null) {
			for (WebLink wl : wlSet) {
				logger.info(" --> WebLink:" + wl.getURI());
			}
		}
	}

	// method of get current URI
	public String getCurrentUri() {
		return (clientConn != null ? clientConn.getURI() : serverAddress);
	}

	// method of ping server
	public void pingServer() {
		logger.info("Sending ping...");
		initClient();
		if (clientConn.ping()) { // if ping() return true means ping successful
			logger.info("Ping successful!");
		}

	}

	// method to send DELETE request
	public void sendDeleteRequest() {
		initClient();
		handleDeleteRequest();
	}

	// method to send DELETE request through resourceName
	public void sendDeleteRequest(String resourceName) {
		isInitialized = false;
		initClient(resourceName);
		handleDeleteRequest();
	}

	// method to send GET request
	public void sendGetRequest() {
		initClient();
		handleGetRequest(false);
	}

	// method to send GET request through resourceName
	public void sendGetRequest(String resourceName) {
		isInitialized = false;
		initClient(resourceName);
		handleGetRequest(false);
	}

	// method of send GET request by using NON
	public void sendGetRequest(boolean useNON) {
		initClient();
		handleGetRequest(useNON);
	}

	// method of send GET request through resourceName and decide to use NON or not
	public void sendGetRequest(String resourceName, boolean useNON) {
		isInitialized = false;
		initClient(resourceName);
		sendGetRequest(useNON);
	}

	// method of send POST request, send payload and decide to useCON or not
	public void sendPostRequest(String payload, boolean useCON) {
		initClient();
		handlePostRequest(payload, useCON);
	}

	// method of send POST request through resourceName, send payload and decide to
	// useCON or not
	public void sendPostRequest(String resourceName, String payload, boolean useCON) {
		isInitialized = false;
		initClient(resourceName);
		handlePostRequest(payload, useCON);
	}

	// method of send PUT request, send payload and decide to useCON or not
	public void sendPutRequest(String payload, boolean useCON) {
		initClient();
		handlePutRequest(payload, useCON);
	}

	// method of send PUT request through resourceName, send payload and decide to
	// useCON or not
	public void sendPutRequest(String resourceName, String payload, boolean useCON) {
		isInitialized = false;
		initClient(resourceName);
		handlePutRequest(payload, useCON);
	}

	// private methods of handle DELETE request
	private void handleDeleteRequest() {
		logger.info("Sending DELETE...");
		CoapResponse response = null; // create a new CoapResponse object
		response = clientConn.delete();
		if (response != null) {
			// log the message to show response information
			logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());
		} else {
			logger.warning("No response received. ");
		}
	}

	// private methods to handle GET request
	private void handleGetRequest(boolean useNON) {
		logger.info("Sending GET...");
		CoapResponse response = null;
		if (useNON) {
			clientConn.useNONs().useEarlyNegotiation(32).get(); // if useNON is true, use NON to send get request
		}
		response = clientConn.get();
		if (response != null) {
			logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());

		} else {
			logger.warning("No response received.");
		}

	}

	// private methods to handle PUT request
	private void handlePutRequest(String payload, boolean useCON) {
		logger.info("Sending PUT...");
		CoapResponse response = null;
		if (useCON) {
			clientConn.useCONs().useEarlyNegotiation(32).get();
		}
		response = clientConn.put(payload, MediaTypeRegistry.TEXT_PLAIN);
		if (response != null) {
			logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());

		} else {
			logger.warning("No response received.");
		}

	}

	// private methods to handle POST request
	private void handlePostRequest(String payload, boolean useCON) {
		logger.info("Sending POST...");
		CoapResponse response = null;
		if (useCON) {
			clientConn.useCONs().useEarlyNegotiation(32).get();
		}
		response = clientConn.post(payload, MediaTypeRegistry.TEXT_PLAIN); // send POST request with payload
		if (response != null) {
			logger.info(
					"Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode()); // log
																														// the
																														// message
																														// to
																														// show
																														// response
																														// information
			logger.warning("No response received.");
		} else {
			logger.warning("No response received.");
		}
	}

	// method of initial client without params
	private void initClient() {
		initClient(null);

	}

	// method of initial client with resourceName
	public void initClient(String resourceName) {

		if (isInitialized) { // if the client is initialized, return
			return;
		}
		if (clientConn != null) {
			clientConn.shutdown(); // if connection have been established, shutdown old connection
			clientConn = null;
		}
		try {
			if (resourceName != null) {
				serverAddress += "/" + resourceName;
			}
			clientConn = new CoapClient(serverAddress);
			logger.info("Created client connection to server / resource: " + serverAddress);

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to connect to Server: " + getCurrentUri(), e);
		}
	}

}
