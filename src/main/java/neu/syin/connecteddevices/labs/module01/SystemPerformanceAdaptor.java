package neu.syin.connecteddevices.labs.module01;

import java.util.logging.Logger;

import com.labbenchstudios.edu.connecteddevices.common.DevicePollingManager;

public class SystemPerformanceAdaptor {
	private static final Logger _Logger = Logger.getLogger(SystemPerformanceAdaptor.class.getSimpleName());
	private DevicePollingManager _pollManager;
	private long _pollCycle;

	/*reference code from professor
	 * implement DevicePollingManager 
	 */
	public SystemPerformanceAdaptor(long pollCycle) {
		super();
		//set interval value
		if (pollCycle >= 1L) {
			_pollCycle = pollCycle;
		}
		//create 2 task
		_pollManager = new DevicePollingManager(2);
	}

	public void startPolling() {
		_Logger.info("Creating and scheduling CPU Utilization poller...");
		_pollManager.schedulePollingTask(new SystemCpuUtilTask("CPU Utilization = ", _pollCycle), _pollCycle);
		_Logger.info("Creating and scheduling Memory Utilization poller...");
		_pollManager.schedulePollingTask(new SystemMemUtilTask("Memory Utilization = ", _pollCycle), _pollCycle);
	}
	
	public void stopPolling() {
		_pollManager.stopPolling();
	}

}
