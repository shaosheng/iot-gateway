package neu.syin.connecteddevices.labs.module01;

import java.lang.management.ManagementFactory;
import java.util.logging.Logger;

import com.sun.management.OperatingSystemMXBean;

public class SystemCpuUtilTask implements Runnable {

	private static final Logger _Logger = Logger.getLogger(SystemCpuUtilTask.class.getSimpleName());

	/*
	 * The management interface for the operating system on which the Java virtual
	 * machine is running. This interface defines several convenient methods for
	 * accessing system properties about the operating system on which the Java
	 * virtual machine is running.
	 */
	private OperatingSystemMXBean osBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);

	private long rateInSec;
	private String description;

	public String DEFAULT_DES = "CPU Usage";
	public long DEFAULT_RATE = 2L;

	public SystemCpuUtilTask() {
		this.description = DEFAULT_DES;
		this.rateInSec = DEFAULT_RATE;
	}

	/*
	 * constructor for task class
	 * 
	 * @param des description print on console
	 * 
	 * @param _pollCycle interval for task
	 */
	public SystemCpuUtilTask(String des, long _pollCycle) {
		// TODO Auto-generated constructor stub
		this.description = des;
		this.rateInSec = _pollCycle;
	}

	/*
	 * Getter and Setter function
	 */
	public long getRateInSec() {
		return rateInSec;
	}

	public void setRateInSec(long rateInSec) {
		this.rateInSec = rateInSec;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public OperatingSystemMXBean getOsBean() {
		return osBean;
	}

	public void setOsBean(OperatingSystemMXBean osBean) {
		this.osBean = osBean;
	}

	public float getDataFromSensor() {
		return (float) osBean.getSystemCpuLoad() * 100;
	}

	public void run() {
		// TODO Auto-generated method stub
		_Logger.info(description + this.getDataFromSensor() + "%");
	}
}
