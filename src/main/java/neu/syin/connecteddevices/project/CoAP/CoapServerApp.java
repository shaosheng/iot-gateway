package neu.syin.connecteddevices.project.CoAP;

public class CoapServerApp {
	private static CoapServerApp App;
	
	public static void main(String[] args) {
		
		App = new CoapServerApp();
		
		try {
			App.start();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	// private var's
	
	private CoapServerConnector coapServer;
	
	// constructor
	
	public CoapServerApp() {
		super();
	}
	
	//public methods of start coap server
	
	public void start() {
		coapServer = new CoapServerConnector();
		coapServer.start();
	}
}
