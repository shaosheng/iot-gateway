package neu.syin.connecteddevices.project.CoAP;

import com.google.gson.Gson;
import neu.syin.connecteddevices.labs.common.DataUtil;
import neu.syin.connecteddevices.labs.common.SensorData;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

public class TempResourceHandler extends CoapResource {

    private static final Logger logger = Logger.getLogger(TempResourceHandler.class.getName());
    // create SensorData instance
    private SensorData sd = new SensorData();
    // create Gson instance
    private Gson gson = new Gson();
    private DataUtil dataUtil = new DataUtil();

    // default constructor
    public TempResourceHandler() {
        super("temp");
    }

    // constructor with params name
    public TempResourceHandler(String name) {
        super(name);
    }

    // constructor with params name and visible
    public TempResourceHandler(String name, boolean visible) {
        super(name, visible);
    }

    @Override
    public void handleGET(CoapExchange exchange) {
        // TODO Auto-generated method stub
        exchange.respond(ResponseCode.VALID, "Get wokred");
        logger.info("Received Get request from client");
    }

    @Override
    public void handlePOST(CoapExchange exchange) {
        // TODO Auto-generated method stub
        String requestText = exchange.getRequestText();
        logger.info("Requset Text : " + requestText);
        sd = dataUtil.jsonToSensorData(requestText);
        logger.info("Transfer JSON into SensorData instance: ");
        logger.info(sd.toString());
        logger.info("Transfer SensorData instance into JSON format: ");
        String payload = gson.toJson(sd);
        logger.info(payload);
        //todo: call aws api
        logger.info("end post request");
        //			handleGetAWS(payload); need catch io exception
        handlePostAWS(payload);
        exchange.respond(ResponseCode.CONTENT, payload);
    }

    @Override
    public void handlePUT(CoapExchange exchange) {
        // TODO Auto-generated method stub
        exchange.respond(ResponseCode.VALID, "Put wokred");
        logger.info("Received Put request from client");
    }

    @Override
    public void handleDELETE(CoapExchange exchange) {
        // TODO Auto-generated method stub
        exchange.respond(ResponseCode.VALID, "Delete wokred");
        logger.info("Received Delete request from client");
    }

    public void handleGetAWS(String payload) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        try {
            String api_url = "https://plxa18vxpj.execute-api.us-east-1.amazonaws.com/stage1";
            HttpGet request = new HttpGet(api_url + "/courses");
            CloseableHttpResponse response = httpClient.execute(request);
            try {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    // return it as a String
                    String result = EntityUtils.toString(entity);
                    System.out.println(result);
                }
            } finally {
                response.close();
            }
        } finally {
            httpClient.close();
        }
    }

    public void handlePostAWS(String payload){
        String api_url = "https://plxa18vxpj.execute-api.us-east-1.amazonaws.com/stage1";
        HttpPost post = new HttpPost(api_url + "/sensordata");
        try {
            post.setEntity(new StringEntity(payload));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try{
            CloseableHttpClient httpClient = HttpClients.createDefault();
            CloseableHttpResponse response = httpClient.execute(post);
            String result = EntityUtils.toString(response.getEntity());
            System.out.println(result);
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
