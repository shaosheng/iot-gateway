package neu.syin.connecteddevices.project.MQTT;

import org.eclipse.paho.client.mqttv3.MqttException;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TempSensorPublisherApp {
	private static final Logger logger = Logger.getLogger(TempSensorPublisherApp.class.getName());
	private static TempSensorPublisherApp app;
	private MqttClientConnector client;
	private String host = "things.ubidots.com";
	private String userName = "BBFF-uhDtFb6UuNlmibMFspKhl9o4TefQg2";
	private String pemFileName = "/Users/shaoshengyin/git/csye6510/iot-gateway/config/ubidots_cert.pem";

	/*
	 * Main function start the app
	 */
	public static void main(String[] args) {
		app = new TempSensorPublisherApp();
		try {
			app.start();
		} catch (Exception e) {
			logger.log(Level.WARNING, "Failed!! to start app.", e);
		}

	}

	/*
	 * the start method to establish connection and set topic generate and publish
	 * payload every minute
	 */
	public void start() throws MqttException {
		// Create a new MqttClient:_Client and connection
		client = new MqttClientConnector(host, userName, pemFileName);
		client.connect();

		// Set topic
		String topic = "/v1.6/devices/csye6510_yin/TempSensor";

		// Set the payload for publishing...
		try {
			while (true) {
				Random r = new Random();
				String payload = String.valueOf(r.nextFloat() * 30);
				System.out.println("payload :" + payload);

				client.publish(topic, 2, payload.getBytes()); // publish payload with the topic in Qoslevel 2
				Thread.sleep(60 * 1000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		client.disconnect();

	}
}
