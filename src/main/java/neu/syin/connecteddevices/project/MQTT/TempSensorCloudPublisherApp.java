package neu.syin.connecteddevices.project.MQTT;

import com.ubidots.ApiClient;
import com.ubidots.DataSource;
import com.ubidots.Variable;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TempSensorCloudPublisherApp {
	private static final Logger logger = Logger.getLogger(TempSensorCloudPublisherApp.class.getName());
	private static TempSensorCloudPublisherApp app;
	private String userName = "BBFF-uhDtFb6UuNlmibMFspKhl9o4TefQg2";
	private String id = "5dbdd7801d847243d6cb8fa3";

	/*
	 * Default constructor
	 */
	public TempSensorCloudPublisherApp() {
		super();
	}

	/*
	 * Main function start the app
	 */
	public static void main(String[] args) {
		app = new TempSensorCloudPublisherApp();
		try {
			app.start();
		} catch (Exception e) {
			logger.log(Level.WARNING, "Failed!! to start app.", e);
		}

	}

	/*
	 * the start method to establish connection and set topic generate and publish
	 * payload every minute
	 */
	public void start() {
		ApiClient apiClient = new ApiClient(userName);
		//this method let client using token, the constructor will not put token in header
		apiClient.fromToken(userName);
		DataSource dataSource = apiClient.getDataSource(id);
		Variable[] variable = dataSource.getVariables();
		try {
			while (true) {
				Random r = new Random();
				float temp = r.nextFloat() * 30;
				System.out.println("Payload: " + temp);
				variable[1].saveValue(temp);
				Thread.sleep(60 * 1000);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
}
